Anoname is just a wrapper and adjective enhancer for Faker, and generates a unique'ish name.
It's original use is a system username generator in my project Arid Grazer, but I thought it would be fun to share as is.

It is deployed on Heroku for demo here: https://obedient-hattie.herokuapp.com/